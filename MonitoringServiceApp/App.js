import React from 'react';
import AddItem from './components/addPerson';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  FlatList, Alert,
  StatusBar,
} from 'react-native';
import { Header } from 'react-native-elements';
import Persons from './components/getPersons';
import AddPerson from './components/addPerson'

const App = () => {  
  return(
    <View style={styles.container}>
      <Header
        style={{backgroundColor: "#00BFFF"}}
        placement="left"
        leftComponent={{ icon: 'menu', color: '#fff' }}
        centerComponent={{ text: 'MonitoringService', style: { color: '#fff' , fontWeight:"bold", fontSize:21 } }}
        rightComponent={{ icon: 'more-vert', color: '#fff' }}
      />
      <AddPerson />
      <Persons/>     
    </View>
  );
};
const styles = StyleSheet.create({
    container: {
    },
});

export default App;