
import React, { PureComponent } from 'react';
import { View,Image,  FlatList, TouchableOpacity, Text, ActivityIndicator , ScrollView  } from 'react-native';
import axios from 'axios';
import styles from './styles';

export default class getPersons extends PureComponent {
    state = {
        personsList: [],
        loading: true
    }
    async componentDidMount() {
        axios.get('https://reqres.in/api/users?page=2').then((response) => {
            this.setState({personsList: response.data.data, loading: false});
        })
        .catch((err) => {
            console.log("Error fetching data-----------", err);
        })
    }
    //Define your renderItem method the callback for the FlatList for rendering each item, and pass data as a argument. 
    renderItem(data) {
        return  <ScrollView
                    contentInsetAdjustmentBehavior="automatic"
                    style={styles.scrollView}>
                    <TouchableOpacity style={{backgroundColor: 'transparent'}}>
                    <View  style={styles.listItemContainer}>
                        <Text style={styles.pokeItemHeader}>{data.item.first_name}</Text>
                        <Image source={{uri: data.item.avatar}} 
                                style={styles.pokeImage}/>
                    </View>
                </TouchableOpacity> 
                </ScrollView>
    }
    render() {
        const { personsList, loading } = this.state;
        if(!loading) {
            return <FlatList 
                    data={personsList}
                    renderItem={this.renderItem}
                    keyExtractor={(item) => item.name} 
                    />
        } else {
            return <ActivityIndicator />
        }
    }
}