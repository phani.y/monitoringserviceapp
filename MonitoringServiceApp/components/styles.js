//import styleSheet for creating a css abstraction.
import { StyleSheet } from 'react-native';
import {
    Colors,
  } from 'react-native/Libraries/NewAppScreen';
  

const styles = StyleSheet.create({
    listItemContainer: {
        borderStyle: 'solid',
        borderColor: '#fff',
        borderBottomWidth: 2,
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 15
    },
    pokeItemHeader: {  
        color: 'black',
        fontSize: 17,
    },
    pokeImage: {
        // backgroundColor: 'transparent',
        height: 80,
        width: 80
    },
    input: {
        height: 60,
        padding: 8,
        margin: 5,
      },
      appButtonContainer: {
        elevation: 8,
        width:200,
        backgroundColor: "#00BFFF",
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 12,
        marginHorizontal:70,
        marginVertical:30
      },
      appButtonText: {
        fontSize: 15,
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase"
      },
    btnText: {
        height:70,
        width:70,
        borderRadius: 10,
        borderWidth: 1,
        borderStyle: 'dashed',
        color: 'darkslateblue',
        fontSize: 20,
        paddingTop: 28,
        margin: 5,
        textAlign:"center"
      },
      scrollView: {
        backgroundColor: Colors.lighter,
      },
})

export default styles;