import React from 'react';
import { View, Text, Image, Button , TextInput , TouchableOpacity } from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import styles from './styles';
import axios from 'axios';
import Icon from 'react-native-vector-icons/dist/FontAwesome';



export default class AddPerson extends React.Component {
  state = {
    file: null,
    person_name: '',
    visible: true
  };

handleUploadPhoto = () => {
    const URL = 'http://119.8.125.174:32115/v1/monitor/persons'
    const data = new FormData();
    data.append('file',{
      uri: this.state.file,
      type: 'image/jpeg',
      name: 'image'
    })
    console.log('data', data)
      let photoConfig = {
        header: {
          "Content-Type": "multipart/form-data"
        }
      }
      axios.post(URL, data, photoConfig).then(response => {
        console.log("status-code", response.status)
        if(response.status == 200){
          console.log('upload succes', response);
          alert('Upload success!');
          this.setState({ file: null });
          this.setState({ person_name: '' });
        }
      })
      .catch((error) => {
        console.log('upload error', error);
        alert('Upload failed!');
      });
  };
  addPic () {
    ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: false
      }).then(image => {
        console.log(image);
        this.state.file = image
        alert('image added')
      }).catch(e => console.log('error', e));
  }
  render() {
    return (
      <View>
        <TextInput
            placeholder="Add person..."
            style={styles.input}
            onChangeText={(text) => {this.setState({person_name: text})}}
        />
        <TouchableOpacity onPress={this.addPic.bind(this)}>
            <Text style={styles.btnText}>
                <Icon name="plus" size={15} />
            </Text>
        </TouchableOpacity>
    <TouchableOpacity  onPress={()=> this.handleUploadPhoto()} style={styles.appButtonContainer}>
        <Text style={styles.appButtonText}>Upload</Text>
    </TouchableOpacity>
    </View>
    )
  };
  
}